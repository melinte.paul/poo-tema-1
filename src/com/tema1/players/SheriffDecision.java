package com.tema1.players;

//Contains possible sheriff decisions
public enum SheriffDecision {
    CHECK, NOT_CHECK, BRIBE
}
