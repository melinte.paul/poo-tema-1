package com.tema1.players;

import com.tema1.decks.Sack;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsFactory;

import java.util.ArrayList;
import java.util.List;

import static com.tema1.common.Constants.BRIBE;
import static com.tema1.common.Constants.ILLEGAL_BY_VALUE;
import static com.tema1.common.Constants.ILLEGAL_PENALTY;
import static com.tema1.common.Constants.LEGAL_BY_VALUE;
import static com.tema1.common.Constants.LEGAL_PENALTY;
import static com.tema1.common.Constants.NUMBER_GOODS;
import static com.tema1.common.Constants.NUMBER_LEGAL_GOODS;
import static com.tema1.common.Constants.SACK_SIZE;
import static java.lang.Integer.min;

/**
 * Class describing the bribed strategy.
 */
public final class BribedPlayer extends Player {
    BribedPlayer(final int position) {
        super(position);
    }

    public Sack getSack(final int round) {
        GoodsFactory goodsFactory = GoodsFactory.getInstance();
        int[] noGoods = new int[NUMBER_GOODS];
        ArrayList<Integer> sackGoods = new ArrayList<Integer>();
        int illegalAmount = 0;

        //Calculates maximum illegal goods
        int maxIllegalAmount = min((gold - 1) / ILLEGAL_PENALTY, SACK_SIZE);

        int bribe;

        //If the player would reach 0 gold, they dont add illegal goods
        if (gold <= ILLEGAL_PENALTY + 1) {
            maxIllegalAmount = 0;
        }

        for (Goods good : hand.getGoods()) {
            noGoods[good.getId()]++;
        }

        //Adds illegal goods while able too, in order of their value
        for (int i : ILLEGAL_BY_VALUE) {
            for (int j = 0; j < noGoods[i] && illegalAmount < maxIllegalAmount; j++) {
                sackGoods.add(i);
                illegalAmount++;
            }
        }

        //If not able to add any illegal goods, they use the basic strategy
        if (illegalAmount == 0) {
            if (hand.onlyIllegal() && gold >= ILLEGAL_PENALTY) {
                hand.getGoods().sort((goods, t1) -> t1.getProfit() - goods.getProfit());
                sackGoods.add(hand.getGoods().get(0).getId());

                sack = new Sack(sackGoods, 0);
                return sack;
            }

            int goodType = NUMBER_LEGAL_GOODS - 1;

            for (int i = NUMBER_LEGAL_GOODS - 2; i >= 0; i--) {
                if (noGoods[i] > noGoods[goodType]) {
                    goodType = i;
                } else if (noGoods[i] == noGoods[goodType]
                    && noGoods[i] * goodsFactory.getGoodsById(i).getProfit()
                    > noGoods[goodType] * goodsFactory.getGoodsById(goodType).getProfit()) {
                    goodType = i;
                }
            }

            for (int i = 0; i < noGoods[goodType]; i++) {
                sackGoods.add(goodType);
            }

            sack = new Sack(sackGoods, goodType);
            return sack;
        } else if (illegalAmount <= 2) {
            bribe = BRIBE;
        } else {
            bribe = 2 * BRIBE;
        }

        //Calculates now maximum goods amount, including any legal, undeclared items
        maxIllegalAmount = min(illegalAmount
           + (gold - ILLEGAL_PENALTY * illegalAmount - 1) / LEGAL_PENALTY, SACK_SIZE);

        //Adds legal goods while albe to
        for (int i : LEGAL_BY_VALUE) {
            for (int j = 0; j < noGoods[i] && illegalAmount < maxIllegalAmount; j++) {
                sackGoods.add(i);
                illegalAmount++;
            }
        }

        sack = new Sack(sackGoods, bribe, 0);
        return sack;

    }

    /**
     * Greedy check left and right players if able to, then accepts the bribe of everyone else.
     */
    public SheriffDecision getDecision(final int bribe,
                                       final int playerPos, final int noPlayers) {
        if ((playerPos == (position + 1) % noPlayers)
                || (playerPos == position - 1)
                || (playerPos == (noPlayers - 1) && position == 0)) {
            if (gold >= SACK_SIZE * LEGAL_PENALTY) {
                return SheriffDecision.CHECK;
            }
            return SheriffDecision.NOT_CHECK;
        }

        return SheriffDecision.BRIBE;
    }

    /**
     * First check left, then right players, then the others.
     */
    public List<Integer> getOrder(final int noPlayers) {
        List<Integer> order = new ArrayList<Integer>();

        if (position == 0) {
            order.add((noPlayers - 1));
            for (int i = 1; i < noPlayers - 1; i++) {
                order.add(i);
            }

            return order;
        }

        order.add(position - 1);

        for (int i = position + 1; (i % noPlayers) != position - 1; i++) {
            order.add(i % noPlayers);
        }

        return order;

    }

    String getPlayerType() {
        return "BRIBED";
    }

}
