package com.tema1.players;

import com.tema1.decks.Sack;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsFactory;

import java.util.ArrayList;
import java.util.List;

import static com.tema1.common.Constants.ILLEGAL_BY_VALUE;
import static com.tema1.common.Constants.LEGAL_PENALTY;
import static com.tema1.common.Constants.NUMBER_GOODS;
import static com.tema1.common.Constants.NUMBER_LEGAL_GOODS;
import static com.tema1.common.Constants.SACK_SIZE;

/**
 * Greedy strategy.
 */
public final class GreedyPlayer extends Player {
    public GreedyPlayer(final int position) {
        super(position);
    }

    public Sack getSack(final int round) {
        GoodsFactory goodsFactory = GoodsFactory.getInstance();
        int[] noGoods = new int[NUMBER_GOODS];
        ArrayList<Integer> sackGoods = new ArrayList<Integer>();
        int goodType = 0;

        //Same strategy as basic player

        for (Goods good : hand.getGoods()) {
            noGoods[good.getId()]++;
        }

        if (hand.onlyIllegal()) {
            hand.getGoods().sort((goods, t1) -> t1.getProfit() - goods.getProfit());
            sackGoods.add(hand.getGoods().get(0).getId());
            noGoods[sackGoods.get(0)]--;
        } else {

            goodType = NUMBER_LEGAL_GOODS - 1;

            for (int i = NUMBER_LEGAL_GOODS - 2; i >= 0; i--) {
                if (noGoods[i] > noGoods[goodType]) {
                    goodType = i;
                } else if (noGoods[i] == noGoods[goodType]
                        && noGoods[i] * goodsFactory.getGoodsById(i).getProfit()
                        > noGoods[goodType] * goodsFactory.getGoodsById(goodType).getProfit()) {
                    goodType = i;
                }
            }

            for (int i = 0; i < noGoods[goodType]; i++) {
                sackGoods.add(goodType);
            }
        }

        //On even rounds(uneven when counting from 0) adds an illegal item

        if (round % 2 == 1 && sackGoods.size() < SACK_SIZE) {
            for (int i : ILLEGAL_BY_VALUE) {
                if (noGoods[i] != 0) {
                    sackGoods.add(i);
                    break;
                }
            }
        }

        sack = new Sack(sackGoods, goodType);
        return sack;
    }

    public SheriffDecision getDecision(final int bribe, final int playerPos, final int noPlayers) {
        //Checks for a bribe
        if (bribe != 0) {
            return SheriffDecision.BRIBE;
        }

        //Same decision making as the basic strategy
        if (gold < SACK_SIZE * LEGAL_PENALTY) {
            return SheriffDecision.NOT_CHECK;
        } else {
            return SheriffDecision.CHECK;
        }
    }

    public List<Integer> getOrder(final int noPlayers) {
        List<Integer> order = new ArrayList<Integer>();

        for (int i = position + 1; (i % noPlayers) != position; i++) {
            order.add(i % noPlayers);
        }

        return order;
    }

    String getPlayerType() {
        return "GREEDY";
    }

}
