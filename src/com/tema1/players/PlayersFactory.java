package com.tema1.players;

public final class PlayersFactory {
    private static PlayersFactory instance = null;

    private PlayersFactory() {

    }

    public static PlayersFactory getInstance() {
        if (instance == null) {
            instance = new PlayersFactory();
        }

        return instance;
    }

    public Player getPlayerByType(final PlayersType type, final int position) {
        switch (type) {
            case basic:
                return new BasicPlayer(position);
            case bribed:
                return new BribedPlayer(position);
            case greedy:
                return new GreedyPlayer(position);
            default:
                return null;
        }
    }
}
