package com.tema1.players;

import com.tema1.decks.Sack;
import com.tema1.goods.Goods;
import com.tema1.goods.GoodsFactory;

import java.util.ArrayList;
import java.util.List;

import static com.tema1.common.Constants.LEGAL_PENALTY;
import static com.tema1.common.Constants.NUMBER_GOODS;
import static com.tema1.common.Constants.NUMBER_LEGAL_GOODS;
import static com.tema1.common.Constants.SACK_SIZE;

/**
 * Basic player strategy.
 */
public final class BasicPlayer extends Player {

    BasicPlayer(final int position) {
        super(position);
    }

    public Sack getSack(final int round) {
        GoodsFactory goodsFactory = GoodsFactory.getInstance();
        int[] noGoods = new int[NUMBER_GOODS];
        ArrayList<Integer> sackGoods = new ArrayList<Integer>();

        //If the hand has only illegal goods, the player chooses the best one
        if (hand.onlyIllegal()) {
            hand.getGoods().sort((goods, t1) -> t1.getProfit() - goods.getProfit());
            sackGoods.add(hand.getGoods().get(0).getId());

            sack = new Sack(sackGoods, 0);
            return sack;
        }

        //Generated frequency of goods
        for (Goods good : hand.getGoods()) {
            noGoods[good.getId()]++;
        }

        //Gets most frequent good, with the highest value
        int goodType = NUMBER_LEGAL_GOODS - 1;

        for (int i = NUMBER_LEGAL_GOODS - 2; i >= 0; i--) {
            if (noGoods[i] > noGoods[goodType]) {
                goodType = i;
            } else if (noGoods[i] == noGoods[goodType]
                    && noGoods[i] * goodsFactory.getGoodsById(i).getProfit()
                    > noGoods[goodType] * goodsFactory.getGoodsById(goodType).getProfit()) {
                goodType = i;
            }
        }

        for (int i = 0; i < noGoods[goodType]; i++) {
            sackGoods.add(goodType);
        }

        //Generates and returns the sack
        sack = new Sack(sackGoods, goodType);
        return sack;
    }

    public SheriffDecision getDecision(final int bribe, final int playerPos, final int noPlayers) {
        //If the player can afford to check the bag, they do
        if (gold < LEGAL_PENALTY * SACK_SIZE) {
            return SheriffDecision.NOT_CHECK;
        } else {
            return SheriffDecision.CHECK;
        }
    }

    public List<Integer> getOrder(final int noPlayers) {
        List<Integer> order = new ArrayList<Integer>();

        for (int i = position + 1; (i % noPlayers) != position; i++) {
            order.add(i % noPlayers);
        }

        return order;
    }

    String getPlayerType() {
        return "BASIC";
    }

}
