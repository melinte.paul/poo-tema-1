package com.tema1.decks;

import com.tema1.goods.Goods;
import com.tema1.goods.GoodsType;

import java.util.List;

import static com.tema1.common.Constants.ILLEGAL_PENALTY;
import static com.tema1.common.Constants.LEGAL_PENALTY;

/**
 * Card container which represents the sack of each player.
 */
public final class Sack extends Deck {
    private int bribe;
    private int penalty;
    private int declared;

    public Sack(final List<Integer> goodsArray, final int declared) {
        super(goodsArray);
        bribe = 0;
        this.declared = declared;

        //Calculates the penalty for the cards
        //If the penalty is positive, it would be payed by the player to the sheriff
        //If it is negative, the sheriff would pay the player
        for (Goods good : goods) {
            if (good.getType() == GoodsType.Illegal) {
                penalty += ILLEGAL_PENALTY;
                continue;
            }

            if (good.getId() != declared) {
                penalty += LEGAL_PENALTY;
            }
        }

        if (penalty == 0) {
            penalty = -LEGAL_PENALTY * goods.size();
        }
    }

    public Sack(final List<Integer> goodsArray, final int bribe, final int declared) {
        this(goodsArray, declared);
        this.bribe = bribe;
    }

    public int getBribe() {
        return bribe;
    }

    public int getPenalty() {
        return penalty;
    }

    int getDeclared() {
        return declared;
    }
}
