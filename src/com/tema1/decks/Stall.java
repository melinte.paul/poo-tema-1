package com.tema1.decks;

import com.tema1.goods.Goods;
import com.tema1.goods.GoodsType;
import com.tema1.goods.IllegalGoods;

import java.util.ArrayList;
import java.util.Map;

import static com.tema1.common.Constants.NUMBER_LEGAL_GOODS;

/**
 * Container representing the stall of a player.
 */
public final class Stall extends Deck {
    public Stall() {
        super();
    }

    /**
     * Adds a sack to the stall.
     * If it was checked, it add only the declared goods.
     * @param sack the added sack.
     * @param checked true if it was checked.
     */
    public void addSack(final Sack sack, final boolean checked) {
        for (Goods good : sack.getGoodsList()) {
            if (checked && good.getId() == sack.getDeclared()) {
                goods.add(good);
            } else if (!checked) {
                goods.add(good);
            }
        }
    }

    /**
     * Used at the end to add bonus cards from illegal goods.
     */
    public void addBonus() {
        Map<Goods, Integer> bonus;
        ArrayList<Goods> bonusGoods = new ArrayList<Goods>();
        for (Goods good : goods) {
            if (good.getType().equals(GoodsType.Illegal)) {
                bonus = ((IllegalGoods) good).getIllegalBonus();

                for (Map.Entry<Goods, Integer> bonusGood : bonus.entrySet()) {
                    for (int i = 0; i < bonusGood.getValue(); i++) {
                        bonusGoods.add(bonusGood.getKey());
                    }
                }
            }
        }

        goods.addAll(bonusGoods);
    }

    /**
     * @return score before king/queen bonuses and gold.
     */
    public int getScore() {
        int score = 0;
        for (Goods good : goods) {
            score += good.getProfit();
        }

        return score;
    }

    /**
     * @return array containing the frequency of legal goods on the stall.
     */
    public int[] getLegalAmount() {
        int[] goodsAmount = new int[NUMBER_LEGAL_GOODS];

        for (Goods good : goods) {
            if (good.getType().equals(GoodsType.Legal)) {
                goodsAmount[good.getId()]++;
            }
        }

        return goodsAmount;
    }
}
