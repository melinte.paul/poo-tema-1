package com.tema1.decks;

import com.tema1.goods.Goods;
import com.tema1.goods.GoodsFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Base goods container.
 */
public class Deck {
    ArrayList<Goods> goods;

    public Deck() {
        goods = new ArrayList<Goods>();
    }

    public Deck(final List<Integer> goodsArray) {
        GoodsFactory goodsFactory = GoodsFactory.getInstance();

        goods = new ArrayList<Goods>();

        for (Integer good : goodsArray) {
            goods.add(goodsFactory.getGoodsById(good));
        }
    }

    final ArrayList<Goods> getGoodsList() {
        return goods;
    }
}
