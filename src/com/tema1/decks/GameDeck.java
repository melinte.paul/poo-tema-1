package com.tema1.decks;

import java.util.ArrayList;
import java.util.List;

import static com.tema1.common.Constants.HAND_SIZE;

/**
 * Card container which contains the game deck.
 */
public final class GameDeck extends Deck {

    public GameDeck(final List<Integer> goodsList) {
        super(goodsList);
    }

    /**
     * @return A list of 10 cards which make a valid hand
     */
    public List<Integer> getHand() {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < HAND_SIZE; i++) {
            list.add(goods.get(0).getId());
            goods.remove(0);
        }

        return list;
    }
}
