package com.tema1.decks;

import com.tema1.goods.Goods;
import com.tema1.goods.GoodsType;

import java.util.ArrayList;
import java.util.List;

/**
 * Card container representing a players hand.
 */
public final class Hand extends Deck {

    public Hand(final List<Integer> goodsArray) {
        super(goodsArray);
    }

    public ArrayList<Goods> getGoods() {
        return goods;
    }

    /**
     * @return Returns true if all cards are illegal
     */
    public boolean onlyIllegal() {
        for (Goods good : goods) {
            if (good.getType().equals(GoodsType.Legal)) {
                return false;
            }
        }

        return true;
    }
}
