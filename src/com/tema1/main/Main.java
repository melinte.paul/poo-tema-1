package com.tema1.main;

public final class Main {
    private Main() {
        // just to trick checkstyle
    }

    /**
     * Starts running the game.
     * @param args holds input file.
     */
    public static void main(final String[] args) {
        GameInputLoader gameInputLoader = new GameInputLoader(args[0], args[1]);
        GameInput gameInput = gameInputLoader.load();

        Game game = new Game(gameInput);
        game.run();
    }
}
