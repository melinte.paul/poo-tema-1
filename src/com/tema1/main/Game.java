package com.tema1.main;

import com.tema1.decks.GameDeck;
import com.tema1.decks.Sack;
import com.tema1.goods.GoodsFactory;
import com.tema1.goods.LegalGoods;
import com.tema1.players.Player;
import com.tema1.players.PlayersFactory;
import com.tema1.players.PlayersType;
import com.tema1.players.SheriffDecision;

import java.util.ArrayList;
import java.util.List;

import static com.tema1.common.Constants.NUMBER_LEGAL_GOODS;

/**
 * Class that runs the game.
 */
final class Game {
    private int rounds;
    private int noPlayers;
    private ArrayList<Player> players;
    private GameDeck deck;

    /**
     * Constructor of Game class.
     * Copies the initial game state from input.
     * @param input is a GameInput instance which contains the parameters of running the game.
     */
    Game(final GameInput input) {
        PlayersFactory playersFactory = PlayersFactory.getInstance();

        rounds = input.getRounds();

        players = new ArrayList<Player>();
        List<String> playersList = input.getPlayerNames();

        for (int i = 0; i < playersList.size(); i++) {
            players.add(playersFactory.getPlayerByType(PlayersType.valueOf(playersList.get(i)), i));
        }

        noPlayers = players.size();

        deck = new GameDeck(input.getAssetIds());
    }

    /**
     * Runs game for a certain number of rounds.
     * At the end prints the scores, including all bonuses.
     */
    void run() {
        for (int i = 0; i < rounds; i++) {
            runRound(i);
        }
        printScore();
    }

    /**
     * Runs each subround.
     * @param roundNumber is used by greedy when deciding to add an illegal item.
     */
    private void runRound(final int roundNumber) {
        for (int i = 0; i < noPlayers; i++) {
            giveCards(i);
            runSubRound(roundNumber, i);
        }
    }

    /**
     * Calculates the result of a certain subround.
     * @param roundNumber is used by greedy when deciding to add an illegal item.
     * @param subRoundNumber is used to decide the sheriff.
     */
    private void runSubRound(final int roundNumber, final int subRoundNumber) {
        Player sheriff = players.get(subRoundNumber);
        Player player;

        //Bribed has a certain checking order, so each player type generates the order
        //they check the other players
        List<Integer> order = sheriff.getOrder(noPlayers);

        for (int i : order) {
            player = players.get(i);

            //The player decides which cards to add to the sack and if they add a bribe
            Sack sack = player.getSack(roundNumber);

            //The sheriff makes his decision
            SheriffDecision decision = sheriff.getDecision(sack.getBribe(), i, noPlayers);

            switch (decision) {
                case NOT_CHECK:
                    player.addSackToStall(false);
                    break;
                case CHECK:
                    player.addSackToStall(true);
                    sheriff.changeGold(sack.getPenalty());
                    player.changeGold(-sack.getPenalty());
                    break;
                case BRIBE:
                    player.addSackToStall(false);
                    sheriff.changeGold(sack.getBribe());
                    player.changeGold(-sack.getBribe());
                    break;
                default:
                    System.out.println("Error");
                    return;
            }
        }
    }

    private void giveCards(final int sheriffNumber) {
        for (int i = 0; i < noPlayers; i++) {
            if (i != sheriffNumber) {
                players.get(i).setHand(deck.getHand());
            }
        }
    }

    /**
     * Prints scores.
     */
    private void printScore() {
        int[] score = new int[noPlayers];
        int king;
        int queen;
        int[][] goodsAmount = new int[noPlayers + 1][NUMBER_LEGAL_GOODS];

        GoodsFactory goodsFactory = GoodsFactory.getInstance();

        for (int i = 0; i < noPlayers; i++) {
            Player player = players.get(i);

            player.addBonus();
            score[i] += player.getBaseScore();
            goodsAmount[i] = player.getStall();
        }

        //Calculation of king and queen bonuses
        for (int i = 0; i < NUMBER_LEGAL_GOODS; i++) {
            king = noPlayers;
            queen = noPlayers;

            for (int j = 0; j < noPlayers; j++) {
                if (goodsAmount[j][i] > goodsAmount[king][i]) {
                    queen = king;
                    king = j;
                } else if (goodsAmount[j][i] > goodsAmount[queen][i]) {
                    queen = j;
                }
            }
            if (goodsAmount[king][i] != 0) {
                score[king] += ((LegalGoods) goodsFactory.getGoodsById(i)).getKingBonus();
            }
            if (goodsAmount[queen][i] != 0) {
                score[queen] += ((LegalGoods) goodsFactory.getGoodsById(i)).getQueenBonus();
            }
        }

        for (int i = 0; i < noPlayers; i++) {
            players.get(i).setFinalScore(score[i]);
        }

        players.sort((player, t1) -> t1.getFinalScore() - player.getFinalScore());

        for (Player player : players) {
            player.printScore();
        }
    }
}
