package com.tema1.common;

/**
 * Class contains public static values, used throughout the program.
 */
public final class Constants {
    private Constants() {

    }
    //Used to define the LEGAL_BY_VALUE and ILLEGAL_BY_VALUE static vectors
    public static class IllegalGoodsIds {
        static final int SILK = 20;
        static final int PEPPER = 21;
        static final int BARREL = 22;
        static final int BEER = 23;
        static final int SEAFOOD = 24;
    }

    public static class LegalGoodsIds {
        static final int APPLE = 0;
        static final int CHEESE = 1;
        static final int BREAD = 2;
        static final int CHICKEN = 3;
        static final int TOMATO = 4;
        static final int CORN = 5;
        static final int POTATO = 6;
        static final int WINE = 7;
        static final int SALT = 8;
        static final int SUGAR = 9;
    }

    //Used whenever a good is chosen by value
    public static final int[] LEGAL_BY_VALUE = {LegalGoodsIds.WINE,
            LegalGoodsIds.CHICKEN, LegalGoodsIds.BREAD, LegalGoodsIds.SUGAR,
            LegalGoodsIds.POTATO, LegalGoodsIds.TOMATO, LegalGoodsIds.CHEESE,
            LegalGoodsIds.SALT, LegalGoodsIds.CORN, LegalGoodsIds.APPLE};

    public static final int[] ILLEGAL_BY_VALUE = {IllegalGoodsIds.SEAFOOD,
            IllegalGoodsIds.SILK, IllegalGoodsIds.PEPPER,
            IllegalGoodsIds.BARREL, IllegalGoodsIds.BEER};

    public static final int NUMBER_LEGAL_GOODS = 10;
    public static final int NUMBER_GOODS = 25;

    //Used in the Player constructor
    public static final int STARTING_GOLD = 80;

    //Used whenever checking hand and sack limits
    public static final int SACK_SIZE = 8;
    public static final int HAND_SIZE = 10;

    //Used when calculating penalties
    public static final int ILLEGAL_PENALTY = 4;
    public static final int LEGAL_PENALTY = 2;

    //Used when calculating the bribe
    public static final int BRIBE = 5;



    // add/delete any constants you think you may use
}
