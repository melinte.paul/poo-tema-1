package com.tema1.goods;

public abstract class Goods {
    private final int id;
    private final GoodsType type;
    private final int profit;

    Goods(final int id, final GoodsType type, final int profit) {
        this.id = id;
        this.type = type;
        this.profit = profit;
    }

    public final int getId() {
        return id;
    }

    public final GoodsType getType() {
        return type;
    }

    public final int getProfit() {
        return profit;
    }

}
